﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;

namespace kpz_lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            var vagons = Manager.GetVagonsList();

            //1
            Console.WriteLine("Task 1");
            var trainDictionary = vagons
                .Select(vagon => vagon.Train)
                .Distinct()
                .ToDictionary(
                    train => train,
                    train => new List<Vagon>(vagons
                        .Where(vagon => vagon.Train.Equals(train)).ToList())
                );
            
            //2
            Console.WriteLine("Task 2");
            Console.WriteLine("Train                Vagon");
            foreach (var keyValuePair in trainDictionary)
            {
                Train train = keyValuePair.Key;
                Console.Write("id: {0}, name: {1}     ", train.Id, train.Name);
                var vagonIterator = keyValuePair.Value.GetEnumerator();

                if (vagonIterator.MoveNext())
                {
                    Vagon vagon = vagonIterator.Current;
                    Console.WriteLine(vagon);
                }

                while (vagonIterator.MoveNext())
                {
                    Vagon vagon = vagonIterator.Current;
                    Console.WriteLine("\t\t     " + vagon);
                }

                Console.WriteLine();
            }

            //3
            Console.WriteLine("Task 3");
            var list = vagons.FindAll(vagon => vagon.ModelYear == 2015);
            foreach (var vagon in list)
            {
                Console.WriteLine(vagon.ModelYear);
            }
            Console.WriteLine();

            //4
            Console.WriteLine("Task 4");
            var modifiedList = vagons.Select(vagon => new {vagon.Id, vagon.PlaceCount, vagon.ModelYear}).ToList();
            foreach (var vagon in modifiedList)
            {
                Console.WriteLine("id: {0}, place count {1}, model year {2}",
                    vagon.Id,
                    vagon.PlaceCount,
                    vagon.ModelYear);
            }
            Console.WriteLine();
            
            //5
            Console.WriteLine("Task 5");
            int id = 200;
            var modVagon = modifiedList.FirstOrDefault(vagon => vagon.Id == id);
            if (modVagon == null)
                Console.WriteLine("No vagon with {0} id", id);
            else
                Console.WriteLine("id: {0}, place count {1}, model year {2}",
                    modVagon.Id,
                    modVagon.PlaceCount,
                    modVagon.ModelYear);
            Console.WriteLine();
            
            //6
            Console.WriteLine("Task 6");
            var vagons1 = vagons
                .Where(vagon => vagon.ModelYear < 2000)
                .Union(vagons.Where(vagon => vagon.ModelYear > 2014))
                .OrderBy(vagon => vagon.ModelYear)
                .GroupBy(vagon => vagon.ModelYear, (modelYear, vagons) => new
                {
                    Key = modelYear,
                    Count = vagons.Count()
                })
                .ToList();
            foreach (var vagon in vagons1)
            {
                Console.WriteLine(vagon.Key);
                Console.WriteLine(vagon.Count);
            }
            Console.WriteLine();

            //7
            Console.WriteLine("Task 7");
            var averageModelYear = vagons.Average(vagon => vagon.ModelYear);
            Console.WriteLine((int)averageModelYear);
            Console.WriteLine();
            
            //8
            Console.WriteLine("Task 8");
            vagons
                .OrderByDescending(vagon => vagon.ModelYear)
                .ThenByDescending(vagon => vagon.Id)
                .ToList()
                .ForEach(Console.WriteLine);
            Console.WriteLine();
        }
    }
}