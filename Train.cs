namespace kpz_lab2
{
    public class Train
    {
        private int id;
        private string name;

        public Train()
        {
        }

        public Train(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public int Id => id;

        public string Name => name;
        
        
    }
}