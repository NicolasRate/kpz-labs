namespace kpz_lab2
{
    public class VagonType
    {
        private int id;
        private string name;

        public VagonType()
        {
        }

        public VagonType(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public int Id => id;

        public string Name => name;
    }
}