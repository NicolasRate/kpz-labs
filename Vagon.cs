using System;

namespace kpz_lab2
{
    public class Vagon
    {
        private int id;
        private Train train;
        private VagonType type;
        private int placeCount;
        private string brand;
        private int modelYear;

        public Vagon()
        {
        }

        public Vagon(int id, Train train, VagonType type, int placeCount, string brand, int modelYear)
        {
            this.id = id;
            this.train = train;
            this.type = type;
            this.placeCount = placeCount;
            this.brand = brand;
            this.modelYear = modelYear;
        }

        public int Id => id;

        public Train Train => train;

        public VagonType Type => type;

        public int PlaceCount => placeCount;

        public string Brand => brand;

        public int ModelYear => modelYear;

        public override string ToString()
        {
            return $"id: {id}, type: {type?.Name}, place count: {placeCount}, brand: {brand}, model year: {modelYear}";
        }
    }
}