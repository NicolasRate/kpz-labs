using System.Collections.Generic;

namespace kpz_lab2
{
    public class Manager
    {
        private static List<Train> trains = new();
        private static List<VagonType> vagonTypes = new();
        private static List<Vagon> vagons = new();

        public static List<Vagon> GetVagonsList()
        {
            trains.Add(new Train(1, "97Z"));
            trains.Add(new Train(2, "65F"));
            trains.Add(new Train(3, "18Y"));
            trains.Add(new Train(4, "18L"));
            trains.Add(new Train(5, "15Q"));
            
            vagonTypes.Add(new VagonType(1, "Cooper"));
            vagonTypes.Add(new VagonType(2, "Magnum"));
            vagonTypes.Add(new VagonType(3, "Mariner"));
            vagonTypes.Add(new VagonType(4, "Colt"));
            vagonTypes.Add(new VagonType(5, "Expedition"));
            
            vagons.Add(new Vagon(1, trains[0], vagonTypes[4], 40, "Maybach", 2015));
            vagons.Add(new Vagon(2, trains[2], vagonTypes[2], 80, "Lexus", 1990));
            vagons.Add(new Vagon(3, trains[4], vagonTypes[0], 45, "Subaru", 1986));
            vagons.Add(new Vagon(4, trains[3], vagonTypes[1], 100, "Honda", 1983));
            vagons.Add(new Vagon(5, trains[1], vagonTypes[3], 85, "Land Rover", 2016));
            vagons.Add(new Vagon(6, trains[1], vagonTypes[3], 70, "Lexus", 2000));
            vagons.Add(new Vagon(7, trains[0], vagonTypes[4], 50, "Toyota", 1997));
            vagons.Add(new Vagon(8, trains[2], vagonTypes[2], 65, "Lincoln", 1996));
            vagons.Add(new Vagon(9, trains[3], vagonTypes[1], 85, "Volvo", 2000));
            vagons.Add(new Vagon(10, trains[1], vagonTypes[3], 90, "Suzuki", 2006));
            vagons.Add(new Vagon(11, trains[2], vagonTypes[2], 105, "Acura", 2011));
            vagons.Add(new Vagon(12, trains[3], vagonTypes[1], 75, "Audi", 2010));
            vagons.Add(new Vagon(13, trains[3], vagonTypes[1], 120, "Chevrolet", 2018));
            vagons.Add(new Vagon(14, trains[4], vagonTypes[0], 80, "Nissan", 2015));
            vagons.Add(new Vagon(15, trains[0], vagonTypes[4], 95, "Ford", 2007));

            return vagons;
        }
    }
}